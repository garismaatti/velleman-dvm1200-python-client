#!/usr/bin/env python

# Copyright (c) Sabin Iacob <iacobs@gmail.com>.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the University nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

__doc__ = """
Usage:

import dvm1200
import serial

port = serial.Serial('/dev/ttyUSB1') # Linux; other operating systems will be 
                                     # different

while True:
    line = dvm1200.read_data(port)
    data = dvm1200.decode(line)

    print data['value'], data['unit'], ', '.join(data['unit_extra'])', '.join(data['flags'])


Gory details:

The multimeter is as dumb as they come when it comes to PC interfacing,
and makes a screen dump to USB (over an IR link) instead of providing proper
ASCII data. The original software is Windows only and doesn't work in Wine.

The output consists of 14 byte packets that look like this when converted to 
hex:

1b 27 3d 4d 5b 65 7b 87 9e a0 b0 c0 d4 e0

The first 4 bits can be discarded, leaving us with

b 7 d d b 5 b 7 e 0 0 0 4 0

Further, these can be grouped like

b 7d db 5b 7e 00 04 0

These encode various things on the screen; in order of appearance:
 * left side flags (AC, DC, PC-Link, AUTO)
 * first digit
 * second digit
 * third digit
 * fourth digit
 * mostly scaling information (nano, micro, milli, etc.), but also diode, beep
 * mostly units (V, A, F, Hz), but also REL, H(old)
 * top flags: MIN, MAX, but also deg. C

The last half byte looks like [MIN, <no idea>, deg.C, MAX]. The digits use a 
non-standard layout (check decode_digit), but are basically seven segment 
displays; the "decimal point" bit of the leftmost digit controls the sign display.
"""

from typing import List, Tuple, Union, Set
from serial import Serial


def read_data(port: Serial) -> List[str]:
    """
    Read 14 bytes from given port and return parsed hex strings.
    Note: hex strings like '4b', not hex values like '\x4b'

    Reads 14 bytes and only return last 4bits, as the first 4 bits can be discarded.

    :param port: Port where to read
    :return: List of parsed hex strings
    """
    raw_data: bytes = port.read(14)
    hex_data: str = raw_data.hex()

    # Check if reading started middle of data and correct it
    # if ok should produce following list from first 4 bits
    # ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e']
    check_list = [hex_data[i] for i in range(0, 28, 2)]
    # now check where is start hex is
    start_position = check_list.index("1")
    if start_position != 0:
        print(
            "[WARN] Reading started from middle of the data! Auto correcting position"
        )
        port.read(start_position)  # discard until good data
        return read_data(port)  # retry

    # parse hex values leaving first 4 bits off
    parsed_hex: List[str] = [
        hex_data[1],  # left indicators
        hex_data[3] + hex_data[5],  # digit 1
        hex_data[7] + hex_data[9],  # digit 2
        hex_data[11] + hex_data[13],  # digit 3
        hex_data[15] + hex_data[17],  # digit 4
        hex_data[19] + hex_data[21],  # scale
        hex_data[23] + hex_data[25],  # units
        hex_data[27],  # top indicators
    ]

    return parsed_hex


def decode_digit(n: Union[str, int]) -> Tuple[int, bool]:
    """
    Parse 8-digit segment from given hex str or int value

              d
             ---
          c | g | h
      --     ---
      a   b |   | f
      .      ---
              e

    :param n: int or hex str (like "4b" instead of hex value "\x4b")
    :return: Segment value and if dot is displayed
    """
    segment_values_table = {
        0b00000101: 1,
        0b01011011: 2,
        0b00011111: 3,
        0b00100111: 4,
        0b00111110: 5,
        0b01111110: 6,
        0b00010101: 7,
        0b01111111: 8,
        0b00111111: 9,
        0b01111101: 0,
        0b01101000: "L",
    }

    # convert hex str to int
    if isinstance(n, str):
        n = int(n, 16)

    # if dot bit set
    has_dot: bool = bool(n & 128)
    # flip dot bit if it was set
    n &= ~128

    # return parsed segment values
    return segment_values_table.get(n), has_dot


def decode_flags(n: Union[str, int]) -> List[str]:
    """
    Decode left side flags
    :param n: hex str from serial
    :return: List of active flags names
    """
    # if hex str convert to int
    if isinstance(n, str):
        n = int(n, 16)

    names: Tuple[str] = ("AC", "DC", "AUTO", "PC")

    # Form list for 4 bit if it is active or not
    flags: List[bool] = [bool(n & (1 << (3 - i))) for i in range(4)]

    # If active add name to the list
    return [names[i] for i in range(4) if flags[i]]


def decode_top_flags(n: Union[str, int]) -> List[str]:
    """
    Decode top flags like MIN and MAX
    :param n: hex str from serial
    :return: List of active flags names
    """
    # if hex str convert to int
    if isinstance(n, str):
        n = int(n, 16)

    names: Tuple[str] = ("MIN", "?", "°C", "MAX")

    # Form list for 4 bit if it is active or not
    flags: List[bool] = [bool(n & (1 << (3 - i))) for i in range(4)]

    # If active add name to the list
    return [names[i] for i in range(4) if flags[i]]


def decode_units(n1: Union[str, int], n2: Union[str, int]) -> List[str]:
    """
    Decode values unit from hex srt or int values.
    :param n1: mostly scaling information (nano, micro, milli, etc.), but also diode, beep
    :param n2: mostly units (V, A, F, Hz), but also REL, H(old)
    :return:
    """
    # if hex str convert to int
    if isinstance(n1, str):
        n1 = int(n1, 16)

    # if hex str convert to int
    if isinstance(n2, str):
        n2 = int(n2, 16)

    scale_names: Tuple[str] = ("u", "n", "k", "Diode", "m", "%", "M", "Beep")
    unit_names: Tuple[str] = ("F", "Ohm", "Relative", "Hold", "A", "V", "Hz", "?")

    fscale: List[bool] = [bool(n1 & (1 << (7 - i))) for i in range(8)]
    funit: List[bool] = [bool(n2 & (1 << (7 - i))) for i in range(8)]

    rscale: List[str] = [scale_names[i] for i in range(8) if fscale[i]]
    runit: List[str] = [unit_names[i] for i in range(8) if funit[i]]

    scales: Set[str] = {"u", "n", "k", "m", "M"}
    units: Set[str] = {"F", "Ohm", "A", "V", "Hz"}

    scale: str = ""
    unit: str = ""
    try:
        unit = units.intersection(runit).pop()
        scale = scales.intersection(rscale).pop()
    except KeyError:
        pass

    return [scale + unit] + list(set(rscale) - scales) + list(set(runit) - units)


def decode_value(line: List[str]) -> Union[float, int]:
    """
    Decode value in digits and scale it to correct scale.
    :param line: list of hex strs
    :return: Shown value
    """
    digit1, digit2, digit3, digit4 = map(decode_digit, line[1:5])

    sign = 1
    multiplier = 1

    if digit1[1]:
        sign = -1
    if digit2[1]:
        multiplier = 0.001
    elif digit3[1]:
        multiplier = 0.01
    elif digit4[1]:
        multiplier = 0.1

    if digit3[0] == "L":
        return 0

    # combine 4 digits into one int
    unscaled_value: int = (
        digit4[0] + 10 * digit3[0] + 100 * digit2[0] + 1000 * digit1[0]
    )

    # Multiply value to correct unit
    value = sign * multiplier * unscaled_value

    # Round value like -48.900000000000006 mV to -48.9 mV
    if isinstance(value, float):
        return float(f"{value:.6}")
    return value


def decode(line: List[str]) -> dict:
    """
    Parse hex str values into dict with readable values
    :param line: parsed hex str value list
    :return: dict with readable values
    """
    flags = decode_flags(line[0])
    flags.extend(decode_top_flags(line[7]))
    units = decode_units(line[5], line[6])

    return {
        "value": decode_value(line),
        "flags": flags,
        "unit": units[0],
        "unit_extra": units[1:],
    }


if __name__ == "__main__":

    port = Serial("/dev/ttyUSB0", 2400, rtscts=True, dsrdtr=True)
    try:
        while True:
            line = read_data(port)
            data = decode(line)
            print(
                data["value"],
                data["unit"],
                ", ".join(data["unit_extra"]),
                ", ".join(data["flags"]),
            )

    except KeyboardInterrupt:
        pass
