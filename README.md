# Velleman(tm) DVM1200 Multimeter python3 client

This is simple python script/library to provide way to read DVM1200 Multimeter serial values
with python3 rather than original Windows(tm) application.

## Requirements
* Python3.8 or newer
* pyserial~=3.5 package installed

## Using
Change port in dvm1200.py to multimeter's serial port in your machine and run.

```bash
python dvm1200.py
```


If hitting permission issues while trying to access serial port,
 try adding yourself to dialout group or run as administrator.
You can also try searching help from internet.


# Legal
Original script by Sabin Iacob
 https://gist.github.com/m0n5t3r/4543215

Modified to python3 by Ari Salopää

Script provided AS IS under MIT lisense.
